"""
Grocery assistant.
Note, it may be only 0-30 apples.
If other quantity is required, the answer is "Столько нет"
"""

import sys
import termcolor


def main():
    """Add "яблоко" word."""
    output_please = "Пожалуйста,"
    try:
        n = int(input("Сколько яблок Вам нужно?\n"))
    except ValueError as ex:
        print("Нужно было ввести целое число!!!")
        print(ex.args)
        sys.exit()
    if n >= 0 and n <= 30:
        if n == 1 or n == 21:
            print(output_please, n, "яблоко")
        elif ((n >= 2 and n <= 4) or (n >= 22 and n <= 24)):
            print(output_please, n, "яблока")
        else:
            print(output_please, n, "яблок")
    elif n < 0 or n > 30:
        print("Столько нет")


if __name__ == "__main__":
    print(termcolor.colored("Grocery assistant", "green"))  # color this caption
    main()
